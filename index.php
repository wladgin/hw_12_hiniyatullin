<?php

$search_words = array('php', 'html', 'интернет', 'Web');

$search_strings = array(
    'Интернет - большая сеть компьютеров, которые могут взаимодействовать друг с другом.',
    'PHP - это распространенный язык программирования  с открытым исходным кодом.',
    'PHP сконструирован специально для ведения Web-разработок и его код может внедряться непосредственно в HTML',
);

function takes_array($search_words, $search_strings)
{
    $result = [];
    for ($i = 0; $i < count($search_strings); $i++) {
        for ($j = 0; $j < count($search_words); $j++) {
            if (preg_match('/' . $search_words[$j] . '/iu', $search_strings[$i])) {
                $result[$i+1][] = $search_words[$j];
            }
        }
    }
    return $result;

}

$results = takes_array($search_words, $search_strings);
foreach ($results as $index => $words) {
    echo "В предложении №{$index} есть слова: " . implode(', ', $words).'<br>';
}
